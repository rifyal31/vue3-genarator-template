import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import * as Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
// import components
import { BootstrapVue3 } from 'bootstrap-vue-3'
import 'bootstrap'
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css'
import Breadcrumb from '@/components/Breadcrumb.vue'
import 'ag-grid-community/styles/ag-grid.css'
import 'ag-grid-community/styles/ag-theme-alpine.css'
import { AgGridVue } from 'ag-grid-vue3'
import Datatable from '@/components/Datatable.vue'
import TextField from '@/components/TextField.vue'
import TextArea from '@/components/TextArea.vue'
import Upload from '@/components/Upload.vue'
import Select from '@/components/Select.vue'
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css'
import ActionButtonForm from '@/components/ActionButtonForm.vue'
import ActionButtonIndex from '@/components/ActionButtonIndex.vue'
import Modal from '@/components/Modal.vue'
import PopupField from '@/components/PopupField.vue'
import MultiPopupField from '@/components/MultiPopupField.vue'

// alertify
import alertify from 'alertifyjs'
window.alertify = alertify

// initial app vue
const app = createApp(App)
app.use(router)
app.mount('#app')
app.use(VueAxios, axios)
app.use(BootstrapVue3)

// declare global compoenent
app.component('Breadcrumb', Breadcrumb)
app.component('AgDatatable', AgGridVue)
app.component('Datatable', Datatable)
app.component('TextField', TextField)
app.component('TextArea', TextArea)
app.component('Select', Select)
app.component('Upload', Upload)
app.component('v-select', vSelect)
app.component('ActionButtonForm', ActionButtonForm)
app.component('ActionButtonIndex', ActionButtonIndex)
app.component('Modal', Modal)
app.component('PopupField', PopupField)
app.component('MultiPopupField', MultiPopupField)
