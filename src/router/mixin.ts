import component from './mixins/component'
import master from './mixins/master'
import transaction from './mixins/transaction'
import { capitalizeLetter } from '@/helpers'
// merge dynamic route
let mixin: any = [...component, ...master, ...transaction]
mixin.forEach((item: any) => {
    // menyusun seq dan group menjadi title panjang
    let kalimat = item.seq + ' ' + item.title

    // mencari kata lain selain dari route group && seq masing"
    const kataArray = kalimat.split(' ')
    const kataSelainTertentu = kataArray.filter((kata) => [item?.group, item?.seq].includes(kata))
    const sisipan = kataSelainTertentu.join(' ')

    // menambahkan kata form jika memiliki params :id
    if (item.original.indexOf(':id') !== -1) {
        kalimat = 'form ' + sisipan
    }
    item.full_title = capitalizeLetter(kalimat)
})
export default mixin
