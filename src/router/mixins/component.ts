const seq = 'component'
const component = [
    {
        seq,
        group: 'dashboard',
        title: 'dashboard',
        original: 'role',
        component: () => import('@/views/pages/dashboard/Dashboard.vue'),
        level: 1
    },
    {
        seq,
        group: 'form',
        title: 'form',
        original: 'form',
        component: () => import('@/views/pages/example/Form.vue'),
        level: 1
    },
    {
        seq,
        group: 'datatable',
        title: 'datatable',
        original: 'datatable',
        component: () => import('@/views/pages/example/Datatable.vue'),
        level: 1
    }
]
export default component
