const seq = 'master'
const master = [
    {
        seq,
        group: 'role',
        title: 'role',
        original: 'role',
        component: () => import('@/views/pages/master/role/Index.vue'),
        level: 1
    },
    {
        seq,
        group: 'role',
        title: 'form role',
        original: 'role/form/:id?',
        component: () => import('@/views/pages/master/role/Form.vue'),
        level: 2
    },
    {
        seq,
        group: 'customer',
        title: 'kustomer',
        original: 'customer',
        component: () => import('@/views/pages/master/customer/Index.vue'),
        level: 1
    },
    {
        seq,
        group: 'customer',
        title: 'form kustomer',
        original: 'customer/form/:id?',
        component: () => import('@/views/pages/master/customer/Form.vue'),
        level: 2
    },
    {
        seq,
        group: 'unit',
        title: 'satuan',
        original: 'unit',
        component: () => import('@/views/pages/master/unit/Index.vue'),
        level: 1
    },
    {
        seq,
        group: 'unit',
        title: 'form satuan',
        original: 'unit/form/:id?',
        component: () => import('@/views/pages/master/unit/Form.vue'),
        level: 2
    },
    {
        seq,
        group: 'product',
        title: 'produk',
        original: 'product',
        component: () => import('@/views/pages/master/product/Index.vue'),
        level: 1
    },
    {
        seq,
        group: 'product',
        title: 'form produk',
        original: 'product/form/:id?',
        component: () => import('@/views/pages/master/product/Form.vue'),
        level: 2
    }
]
export default master
