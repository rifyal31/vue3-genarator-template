const seq = 'transaction'
const transaction = [
    {
        seq,
        group: 'receive_order',
        title: 'receive order',
        original: 'receive_order',
        component: () => import('@/views/pages/transaction/receive_order/Index.vue'),
        level: 1
    },
    {
        seq,
        group: 'receive_order',
        title: 'form receive order',
        original: 'receive_order/form/:id?',
        component: () => import('@/views/pages/transaction/receive_order/Form.vue'),
        level: 2
    }
]
export default transaction
