import { RouteRecordRaw } from 'vue-router'
import { createRouter, createWebHistory } from 'vue-router'
import mixin from './mixin'

const routes: RouteRecordRaw[] = mixin.map((i) => {
    return {
        path: `/${i.seq}/${i.original}`,
        name: `${i.seq}_${i.original}`,
        component: i.component
    }
})

const config = {
    history: createWebHistory(),
    routes
}
const router = createRouter(config)

export default router
