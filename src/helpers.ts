import axios from 'axios'
import mixin from './router/mixin'
import type { RouteLocationNormalized } from 'vue-router'

export const baseApi = () => {
    return import.meta.env.VITE_API_URL + import.meta.env.VITE_API_PATH
}
export const baseApiDefault = () => {
    return import.meta.env.VITE_API_URL
}
export const loadingBG = (status = true) => {
    const loader = document.getElementById('loader') as HTMLDivElement | null
    if (status) {
        loader?.style.setProperty('display', 'block')
    } else {
        loader?.style.setProperty('display', 'none')
    }
}

export function getQueryString(params: any[]) {
    return new URLSearchParams(params).toString()
}

export function axiosApi(
    model: String = '',
    type = 'GET',
    data: any = [],
    header: Object = [],
    loader = true
) {
    if (model) {
        if (loader) loadingBG()
        model = model.replace(baseApi() + '/', '')
        let url = baseApi() + '/' + model
        if (type === 'GET') {
            return axios
                .get(url)
                .then((response) => {
                    if (loader) loadingBG(false)
                    return response?.data
                })
                .catch(({ response }) => {
                    ;(window as any).alertify.error(response?.data?.message)
                    if (loader) loadingBG(false)
                })
        } else if (type === 'POST') {
            return axios
                .post(url, data, header)
                .then((response) => {
                    if (loader) loadingBG(false)
                    return response?.data
                })
                .catch(({ response }) => {
                    ;(window as any).alertify.error(response?.data?.message)
                    if (loader) loadingBG(false)
                })
        } else if (type === 'PUT') {
            return axios
                .put(url, data, header)
                .then((response) => {
                    if (loader) loadingBG(false)
                    return response?.data
                })
                .catch(({ response }) => {
                    ;(window as any).alertify.error(response?.data?.message)
                    if (loader) loadingBG(false)
                })
        } else if (type === 'FORMDATA') {
            header = {
                ...header,
                'Content-Type': 'multipart/form-data'
            }
            data.append('_method', 'PUT')
            return axios
                .post(url, data, header)
                .then((response) => {
                    if (loader) loadingBG(false)
                    return response?.data
                })
                .catch(({ response }) => {
                    ;(window as any).alertify.error(response?.data?.message)
                    if (loader) loadingBG(false)
                })
        } else if (type === 'DELETE') {
            return axios
                .delete(url, data)
                .then((response) => {
                    if (loader) loadingBG(false)
                    return response?.data
                })
                .catch(({ response }) => {
                    ;(window as any).alertify.error(response?.data?.message)
                    if (loader) loadingBG(false)
                })
        } else {
            console.log('METHOD FETCH SALAH')
            loadingBG(false)
        }
    }
}
export function getAllRoute() {
    return mixin
}

export function getAllThisRoute(route: RouteLocationNormalized) {
    console.log(route.name)
    console.log(mixin)
    console.log(mixin.find((i: any) => route.name == `${i.seq}_${i.original}`))
    return mixin.find((i: any) => route.name == `${i.seq}_${i.original}`)
}

export function capitalizeLetter(input: string): string {
    const split = input.split(' ')
    let fix_char = ''
    split.forEach((v) => {
        fix_char += v.charAt(0).toUpperCase() + v.slice(1) + ' '
    })
    return fix_char
}

export function getAllDataFromRefs(thisRef: any, columns: any, useFormData: Boolean = false) {
    let data = {}
    if (!useFormData) {
        // default string value
        for (let i in columns) {
            const val = thisRef[columns[i]]?.getValue() ?? ''
            data = {
                ...data,
                [columns[i]]: val
            }
        }
    } else {
        const formData = new FormData()
        // khusus form upload dengan metode form-data
        for (let i in columns) {
            if (thisRef[columns[i]]?.isUpload) {
                const refUpload = thisRef[columns[i]]
                const valueUpload = refUpload?.getValue()
                formData.append(columns[i], valueUpload)
            } else {
                // other upload field
                const val = thisRef[columns[i]]?.getValue() ?? ''
                formData.append(columns[i], val)
            }
        }
        data = formData
    }
    return data
}

export function setAllDataFromRefs(thisRef: any, columns: any, response: any) {
    let data = {}
    for (let col in columns) {
        for (let i in response) {
            if (i == columns[col]) {
                thisRef[columns[col]]?.setValue(response[i])
            }
        }
        const val = thisRef[columns[col]]?.getValue() ?? ''
        data = {
            ...data,
            [columns[col]]: val
        }
    }
    return data
}
export function getValidDataFromRefs(thisRef: any, columns: any, response: any) {
    let next = true
    for (let i in columns) {
        const val = thisRef[columns[i]]?.getValid()
        if (val == false) console.log(thisRef[columns[i]])
        if (val == false) next = false
    }
    return next
}
export function getImageNotFound() {
    let next = import.meta.env.VITE_API_URL + '/uploads/not-found.jpg'
    return next
}
export function getImage(imagePath: String, state = false) {
    return import.meta.env.VITE_API_URL + '/' + imagePath
}
export function formatThousand(params: any) {
    const number = (params.toString() == '[object Object]' ? params.value : params) ?? 0
    return new Intl.NumberFormat('id-ID').format(currencyToDefault(number))
}
export function formatRPThousand(params: any) {
    const number = (params.toString() == '[object Object]' ? params.value : params) ?? 0
    console.log('NUM ', number)
    const formattedRupiah = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR'
    }).format(currencyToDefault(number))
    console.log('SINI ', formattedRupiah)
    return formattedRupiah
}
export function thousandToDefault(params: any) {
    if (params) {
        const number = params.toString()?.replace(/\./g, '')
        return parseFloat(number)
    } else {
        return 0
    }
}
export function rpToDefault(params: any) {
    if (params) {
        const regex = /\.00\b/
        if (params.toString().includes(',') || regex.test(params.toString())) {
            // mengandung koma ex: Rp. 30.000,00
            const regex = /[^0-9,.]/g
            const cleanedText = params.toString().replace(regex, '')
            console.log('SINI ', cleanedText)
            return parseFloat(cleanedText.replace('.', ''))
        } else {
            // tidak mengandung koma ex: Rp. 30.000
            const numericString = params.toString()?.replace(/[^0-9]/g, '') ?? 0 // Remove non-numeric characters
            return parseInt(numericString)
        }
    } else {
        return 0
    }
}
export function currencyToDefault(params: any) {
    if (params) {
        const regex1 = /\.00\b/
        const regex2 = /\,00\b/
        if (regex1.test(params.toString())) {
            // mengandung koma ex: Rp. 30.000.00
            const regex = /[^0-9,.]/g
            const cleanedText = params.toString().replace(regex, '')
            return parseFloat(cleanedText.replace(/\,/g, ''))
        } else if (regex2.test(params.toString())) {
            // mengandung koma ex: Rp. 30.000,00
            const regex = /[^0-9,.]/g
            const cleanedText = params.toString().replace(regex, '')
            return parseFloat(cleanedText.replace(/\./g, ''))
        } else {
            // tidak mengandung koma ex: Rp. 30.000
            const numericString = params.toString()?.replace(/[^0-9]/g, '') ?? 0 // Remove non-numeric characters
            return parseInt(numericString)
        }
    } else {
        return 0
    }
}
